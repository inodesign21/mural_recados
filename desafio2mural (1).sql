-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 
-- Versão do Servidor: 5.5.24-log
-- Versão do PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `desafio2mural`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mural_recados`
--

CREATE TABLE IF NOT EXISTS `mural_recados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` text NOT NULL,
  `email` varchar(80) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `titulo` text NOT NULL,
  `texto` text NOT NULL,
  `status` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `mural_recados`
--

INSERT INTO `mural_recados` (`id`, `nome`, `email`, `data`, `hora`, `titulo`, `texto`, `status`) VALUES
(1, 'tiago silva', 'tiagonzs@hotmail.com', '2015-02-09', '13:21:35', 'titulo teste', 'texto teste', 'aprovado'),
(2, 'maisteste', 'tiagonzs@hotmail.com', '2015-02-09', '13:27:54', 'maistitulo', 'textomais', 'aguardando'),
(3, 'tiago', 'tiagonzs@hotmail.com', '2015-02-09', '13:51:46', 'titulotestetiago', 'textotestetiago', 'aguardando'),
(4, 'Tiago R.', 'tiagonzs@hotmail.com', '2015-02-09', '14:20:00', 'TÃ­tulo teste novo', 'Mais um recado teste', 'aguardando'),
(5, 'Novo Nome', 'tiagonzs@hotmail.com', '2015-02-09', '14:23:05', 'Novo tÃ­tulo teste', 'Novo texto teste ', 'aprovado'),
(6, 'Novo Nome', 'tiagonzs@hotmail.com', '2015-02-09', '14:24:31', 'Novo tÃ­tulo teste', 'Novo texto teste ', 'aprovado'),
(7, 'Novo Nome', 'tiagonzs@hotmail.com', '2015-02-09', '14:25:31', 'Novo tÃ­tulo teste', 'Novo texto teste ', 'aprovado'),
(8, 'Tiago Teste', 'tiagonzs@hotmail.com', '2015-02-09', '14:32:25', 'Titulo teste novo', 'Texto teste novo', 'aprovado'),
(9, 'Tiago Teste', 'tiagonzs@hotmail.com', '2015-02-09', '14:32:55', 'Titulo teste novo', 'Texto teste novo', 'aprovado'),
(10, 'Tiago Teste', 'tiagonzs@hotmail.com', '2015-02-09', '14:33:29', 'Titulo teste novo', 'Texto teste novo', 'aprovado'),
(12, 'Tiago Teste', 'tiagonzs@hotmail.com', '2015-02-09', '14:34:53', 'Titulo teste novo', 'Texto teste novo', 'aprovado'),
(13, 'Tiago Teste', 'tiagonzs@hotmail.com', '2015-02-09', '14:35:07', 'Titulo teste novo', 'Texto teste novo', 'aprovado'),
(14, 'Tiago Roberto', 'tiagonzs@hotmail.com', '2015-02-09', '16:53:33', 'Mural de recados', 'Mural funcionando', 'aprovado'),
(15, 'Oliveira Junior', 'tiagonzs@hotmail.com', '2015-02-09', '17:04:21', 'Recado do Oliveira', 'Texto do Oliveira', 'aprovado'),
(16, 'Oliveira Junior', 'tiagonzs@hotmail.com', '2015-02-09', '17:05:10', 'Recado do Oliveira', 'Texto do Oliveira', 'aprovado'),
(17, 'Roberto Justo', 'tiagonzs@hotmail.com', '2015-02-09', '17:56:23', 'Titulo do Roberto', 'Recado do Roberto', 'aprovado'),
(18, 'Roberto Justo', 'tiagonzs@hotmail.com', '2015-02-09', '17:59:10', 'Titulo do Roberto', 'Recado do Roberto', 'aguardando'),
(21, 'Joao Maria', 'tiagonzs@hotmail.com', '2015-02-09', '18:16:57', 'Titulo do Joao', 'Texto do Joao', 'aguardando'),
(23, 'Tiago Silva', 'tiagonzs@hotmail.com', '2015-02-09', '21:05:38', 'titulo tiagonzs', 'texto tiagonzs', 'aprovado');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

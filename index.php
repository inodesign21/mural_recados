<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

      <!-- php conectando banco de dados e verificando paginação -->
      <?php 

      $host = "localhost"; 
      $banco = "desafio2mural"; 
      $usuario = "root"; 
      $senha = ""; 
      $conexao = mysql_connect($host, $usuario, $senha) or die ("Erro: ".mysql_error()); 
      $db = mysql_select_db($banco);

      //Busca no banco de dados para contagem
      $sql_res=mysql_query("SELECT * FROM mural_recados WHERE status = 'aprovado'"); //consulta no banco
      $contador=mysql_num_rows($sql_res); //Pegando Quantidade de itens
      //Verificando se já selecionada alguma página
      if(empty($_GET['pag'])){
        $pag=1;
      }else{
        $pag = "$_GET[pag]";} //Pegando página selecionada na URL
      if($pag >= '1'){
        $pag = $pag;
      }else{
        $pag = '1';
      }
      $maximo=5; //Quantidade Máxima de posts por página
      $inicio = ($pag * $maximo) - $maximo; //Variável para LIMIT da sql
      $paginas=ceil($contador/$maximo); //Quantidade de páginas 
      ?>


        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Desafio - Mural de Recados</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Seção envio de recado -->
    <div class="jumbotron">
      <div class="container">
        <h1>Mural de Recados</h1>
        <p>Envie seu recado.</p>  
        <?php 
          if(isset($retorno)) echo "$retorno"
        ?>
        <form action="" method="post" novalidate="">
          <div class="form-group">
            <label for="nome">Nome</label>
            <input name="nome" type="text" class="form-control" id="nome" placeholder="Nome" autofocus="autofocus">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input name="email" type="text" title="email" class="form-control" id="email" placeholder="Email" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" autofocus="autofocus">
          </div>
          <div class="form-group">
            <label for="titulo">Título</label>
            <input name="titulo" type="text" class="form-control" id="titulo" placeholder="Título" autofocus="autofocus">
          </div>
          <div class="form-group">
            <label for="texto">Texto</label>
            <textarea name="texto" class="form-control" rows="3" id="texto" autofocus="autofocus"></textarea>
          </div>
          <button type="submit" name="enviar" value="Enviar Recado "class="btn btn-default">Enviar</button>
          <button class="btn" type="reset">Limpar</button>
        </form>
     

      <!-- php envio de recado ao clicar em enviar -->
      <?php  if(isset($_POST['enviar'])) {

      $status = 'aguardando';
      $data = date('Y-m-d'); 
      $hora = strftime("%H:%M:%S"); 

      $nome = $_POST['nome']; 
      $email = $_POST['email'];
      $titulo = $_POST['titulo']; 
      $texto = $_POST['texto']; 

      if(empty($nome)) {
        $retorno = "Digite seu nome";
      } elseif (empty($email)) {
        $retorno = "Digite seu email";
      } elseif (empty($titulo)) {
        $retorno = "Digite o título";
      } elseif (empty($texto)) {
        $retorno = "Digite o recado";
      } if(empty($retorno)) {

      $insert = "INSERT INTO mural_recados (id, nome, email, titulo, texto, data, hora, status) VALUES ('', '$nome', '$email', '$titulo', '$texto', '$data', '$hora', '$status')"; 
        if($insert >= '1') {
          echo "<br /> <div class='container bg-warning'>Seu recado será moderado</div>";
        } else {
          $retorno = "Erro ao enviar recado";
        }
      

      mysql_query($insert) or die ("NÃO FOI POSSIVEL INSERIR OS DADOS"); 

     // echo "<br /><div class='container bg-success'>O envio foi realizado com sucesso! $data, $hora</div>";

      }
      }
      ?>

       </div>
    </div>

    <div class="container">
      <div class="row">
        <!-- mostrando recados -->
        <h1>Seção dos recados aprovados:</h1>

        <?php 

          $selecaotodos = mysql_query("SELECT * FROM mural_recados WHERE status = 'aprovado'");
          $selecao = mysql_query("SELECT * FROM mural_recados WHERE status = 'aprovado' ORDER BY id DESC LIMIT $inicio,$maximo");
          $conta = mysql_num_rows($selecao);
          $contatodos = mysql_num_rows($selecaotodos);
          

          while($resultado = mysql_fetch_array($selecao)) {
            
            $pessoa = $resultado['nome'];
            $ce = $resultado['email'];
            $txtp = $resultado['titulo'];
            $txt = $resultado['texto'];
            $dt = $resultado['data'];
            $hr = $resultado['hora'];
          
         ?>

      <div class="container mural_recados">
        <h3>Nome: <?php echo $pessoa; ?> <h5><?php echo $dt; ?> <?php echo $hr; ?><h5> </h3>
        <h4>Email: <?php echo $ce; ?></h4>
        <h4>Título: <?php echo $txtp; ?></h4>
        <h4>Texto: <?php echo $txt; ?></h4>
      </div>

      <?php } ?>
      </div>
    </div>

    <!-- mostrando paginação dos recados -->
    <div id="pagi" class="container">
      <nav>
        <ul class="pagination">
          
        <?php
        echo '<h4>Já foram aprovados <strong>'.$contatodos.' </strong>recados</h4>';

          if($pag!=1){
            echo "<li><a href='index.php?pag=".($pag-1)."'> Página Anterior</a></li>"; 
          }
          if($contador<=$maximo){
            echo "<li>Existe apenas uma única página</li>";
          }
          else{
            for($i=1;$i<=$paginas;$i++){
              if($pag==$i){
                echo "<li  class='active'><a href='index.php?pag=".$i."'> ".$i."</a></li>";
              }else{
                echo "<li><a href='index.php?pag=".$i."'> ".$i."</a></li>";
              }
            }
          }
          if($pag!=$paginas){
            echo "<li><a href='index.php?pag=".($pag+1)."'> Próxima Página</a></li> <br /> Paginação de 5 em 5";
          }
        ?>
     
       </ul>
      </nav>    
    </div>

    <div><p>&nbsp</p></div>

    <!-- adm -->
    <div class="jumbotron">
      <div class="container">
        <p>Acesse o <button type="button" class="btn btn-default"><a href="moderar.php" target="_blank">Painel de Controle</a></button> para administrar os recados.</p>
      </div> 
    </div>

      <hr>

      <footer>
        <p>&copy; 2015</p>
      </footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    </body>
</html>

<?php 
      $host = "localhost"; 
      $banco = "desafio2mural"; 
      $usuario = "root"; 
      $senha = ""; 
      $conexao = mysql_connect($host, $usuario, $senha) or die ("Erro: ".mysql_error()); 
      $db = mysql_select_db($banco);
?>



<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/filterbootstrap/theme.default.css">


        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>

      <?php 
            if(isset($_POST['moderar']) && $_POST['moderar'] == 'aceitar') {
              $aceita = mysql_query(" UPDATE mural_recados SET status = 'aprovado' WHERE id = '$_POST[id]' ");
            }
       ?>

       <?php 
            if(isset($_POST['moderar']) && $_POST['moderar'] == 'deletar') {
              $deleta = mysql_query("DELETE FROM mural_recados WHERE id = '$_POST[id]' ");
            }
       ?>

       <?php 
            if(isset($_POST['moderar']) && $_POST['moderar'] == 'reprovar') {
              $reprova = mysql_query(" UPDATE mural_recados SET status = 'aguardando' WHERE id = '$_POST[id]' ");
            }
       ?>

        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Desafio - ADM</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Seção recados a moderar -->
    <div class="adm">
    <div class="container">
      <div class="row">
        <!-- mostrando recados -->
        <h1>Seção dos recados para moderar:</h1>
        <table class="table table-bordered tablesorter" id="tabela-moderar">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Título</th>
                        <th>Recado</th>
                        <th>Data</th>
                        <th>Hora</th>
                        <th data-sorter="false">Aprovar</th>
                        <th data-sorter="false">Deletar</th>
                    </tr>
                </thead>
                <tbody>
        <?php 

          $selecao = mysql_query("SELECT * FROM mural_recados WHERE status = 'aguardando'");

          while($resultado = mysql_fetch_array($selecao)) {
            
            $id = $resultado['id'];
            $pessoa = $resultado['nome'];
            $ce = $resultado['email'];
            $txtp = $resultado['titulo'];
            $txt = $resultado['texto'];
            $dt = $resultado['data'];
            $hr = $resultado['hora'];
          
         ?>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $pessoa; ?></td>
                <td><?php echo $ce; ?></td>
                <td><?php echo $txtp; ?></td>
                <td><?php echo $txt; ?></td>
                <td><?php echo $dt; ?></td>
                <td><?php echo $hr; ?></td>
                <td><form action="" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="id" value="<?php echo $id;?>" />
                  <input type="hidden" name="moderar" value="aceitar" />
                  <input class="btn btn-success" type="submit" value="Aceitar">
                </form></td>
                <td><form action="" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="id" value="<?php echo $id;?>" />
                  <input type="hidden" name="moderar" value="deletar" />
                  <input class="btn btn-danger" type="submit" value="DELETAR">
                </form></td>
            </tr>

             <?php } ?>
          </tbody>
        </table>

      </div>
    </div>
    </div>

  <!-- Seção recados aprovados -->
    <div class="apv">
    <div class="container">
      <div class="row">
        <!-- mostrando recados -->
        <h1>Seção dos recados já aprovados:</h1>
        <table class="table table-bordered tablesorter" id="tabela-aprovados">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Título</th>
                        <th>Recado</th>
                        <th>Data</th>
                        <th>Hora</th>
                        <th data-sorter="false">Reprovar</th>
                    </tr>
                </thead>
                <tbody>
        <?php 

          $selecao = mysql_query("SELECT * FROM mural_recados WHERE status = 'aprovado'");

          while($resultado = mysql_fetch_array($selecao)) {
            
            $id = $resultado['id'];
            $pessoa = $resultado['nome'];
            $ce = $resultado['email'];
            $txtp = $resultado['titulo'];
            $txt = $resultado['texto'];
            $dt = $resultado['data'];
            $hr = $resultado['hora'];
          
         ?>
            <tr>
                <td><?php echo $id; ?></td>
                <td><?php echo $pessoa; ?></td>
                <td><?php echo $ce; ?></td>
                <td><?php echo $txtp; ?></td>
                <td><?php echo $txt; ?></td>
                <td><?php echo $dt; ?></td>
                <td><?php echo $hr; ?></td>
                <td><form action="" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="id" value="<?php echo $id;?>" />
                  <input type="hidden" name="moderar" value="reprovar" />
                  <input class="btn btn-warning" type="submit" value="Reprovar">
                </form></td>
            </tr>

             <?php } ?>
          </tbody>
        </table>

      </div>
    </div>
    </div>

    <div><p>&nbsp</p></div>


      <hr>

      <footer>
        <p>&copy; 2015 - <a href="index.php">Voltar</a></p>
      </footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <!-- plugin jquery de filtrar ordem em tabelas com dados -->
        <script src="js/vendor/jquery.tablesorter.js"></script>
        <script src="js/vendor/jquery.tablesorter.widgets.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    </body>
</html>

$(function() {

	// mouse over nas linhas da tabela

      $('#tabela-moderar tbody tr').mouseover(function() {
         $(this).addClass('seleciona-linha');
      }).mouseout(function() {
         $(this).removeClass('seleciona-linha');
      });

      $('#tabela-aprovados tbody tr').mouseover(function() {
         $(this).addClass('seleciona-linha');
      }).mouseout(function() {
         $(this).removeClass('seleciona-linha');
      });


  // filtro bootstrap

  		$("#tabela-moderar").tablesorter({
		    widgets: ["filter"],
		    widgetOptions : {
		      filter_searchFiltered : false
		    }
		  });
      

      $("#tabela-aprovados").tablesorter({
		    widgets: ["filter"],
		    widgetOptions : {
		      filter_searchFiltered : false
		    }
		  });


   });